#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors



################################################################################
# Check for old mechanism
################################################################################

if [ -n "${HDF5}" ]; then
    echo 'BEGIN ERROR'
    echo "Setting the option \"HDF5\" is incompatible with the HDF5 thorn. Please remove the option HDF5=${HDF5}."
    echo 'END ERROR'
    exit 1
fi


################################################################################
# Configure Cactus
################################################################################

pkg-config --exists hdf5 || exit 1
HDF5_INC_DIRS=$(pkg-config --cflags-only-I hdf5 | sed 's/-I//g')
HDF5_LIB_DIRS=$(pkg-config --libs-only-L hdf5 | sed 's/-L//g')
HDF5_LIBS=$(pkg-config --libs-only-l hdf5 | sed 's/-l//g')
#echo ${HDF5_INC_DIRS}
#echo ${HDF5_LIB_DIRS}
#echo ${HDF5_LIBS}

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_HDF5           = 1"
echo "HDF5_DIR            = ${HDF5_DIR}"
echo "HDF5_ENABLE_CXX     = 0"
echo "HDF5_ENABLE_FORTRAN = ${HDF5_ENABLE_FORTRAN}"
echo "HDF5_INC_DIRS       = ${HDF5_INC_DIRS}"
echo "HDF5_LIB_DIRS       = ${HDF5_LIB_DIRS}"
echo "HDF5_LIBS           = ${HDF5_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(HDF5_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(HDF5_LIB_DIRS)'
echo 'LIBRARY           $(HDF5_LIBS)'
